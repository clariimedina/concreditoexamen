package concreditoExamen;

import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GoogleImageTest {
	
	private WebDriver driver;
	private String urlGoogle = "https://www.google.com";
	
	@Before
	public void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver", "./src/test/resources/chromedriver/chromedriver" );
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get(urlGoogle);
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

	@Test
	public void test() {
		WebElement searchBox = driver.findElement(By.name("q"));
		
		searchBox.clear();
		searchBox.sendKeys("concredito");
		searchBox.submit();
		
		WebElement imgButton = driver.findElement(By.xpath("/html/body/div[7]/div/div[3]/div/div[1]/div/div[1]/div/div[3]/a"));
		imgButton.click();

		WebElement imageResult = driver.findElement(By.xpath("/html/body/div[2]/c-wiz/div[3]/div[1]/div/div/div/div[1]/div[1]/span/div[1]/div[1]/div[1]/a[1]"));
		imageResult.click();
		
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		WebElement closeImageButton = driver.findElement(By.xpath("/html/body/div[2]/c-wiz/div[3]/div[2]/div[3]/div/div/div[2]/a"));
		closeImageButton.click();
		
		WebElement imageShowContainer = driver.findElement(By.xpath("/html/body/div[2]/c-wiz/div[3]/div[2]/div[3]"));
		
		imageShowContainer.isDisplayed();
	}

}
