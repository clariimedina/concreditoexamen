package concreditoExamen;

import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import concreditoExamenPom.GooglePage;

public class GoogleSecondResults {
	
	private WebDriver driver;
	private String urlGoogle = "https://www.google.com";
	private GooglePage googlePage;
	
	@Before
	public void setUp() throws Exception {
		googlePage = new GooglePage(driver);
		driver = googlePage.chromeWebDriverConnection();
		driver.manage().window().maximize();
		driver.get(urlGoogle);
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

	@Test
	public void test() {
		googlePage.searchSomething();
		
		WebElement firstResultFirstPage = driver.findElement(By.className("yuRUbf"));
		String firstResultFirstPageText = firstResultFirstPage.findElement(By.tagName("a")).getText();
		
		WebElement linkNext = driver.findElement(By.id("pnnext"));
		linkNext.click();
		
		WebElement firstResultSecondPage = driver.findElement(By.className("yuRUbf"));
		String firstResultSecondPageText = firstResultSecondPage.findElement(By.tagName("a")).getText();
		
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		assertNotSame(firstResultFirstPageText, firstResultSecondPageText);
	}

}
