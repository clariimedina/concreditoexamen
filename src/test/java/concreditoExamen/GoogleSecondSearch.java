package concreditoExamen;

import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import concreditoExamenPom.GooglePage;

public class GoogleSecondSearch {
	
	private WebDriver driver;
	private String urlGoogle = "https://www.google.com";
	private GooglePage googlePage;

	@Before
	public void setUp() throws Exception {
		googlePage = new GooglePage(driver);
		driver = googlePage.chromeWebDriverConnection();
		driver.manage().window().maximize();
		driver.get(urlGoogle);
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

	@Test
	public void test() {
		googlePage.searchSomething();
		String firstSearchUrl = driver.getCurrentUrl();
		
		WebElement searchBox2 = driver.findElement(By.name("q"));
		searchBox2.click();
		searchBox2.clear();
		searchBox2.sendKeys("concredito ubicacion");
		searchBox2.submit();
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		assertNotSame(firstSearchUrl , driver.getCurrentUrl());
	}

}
