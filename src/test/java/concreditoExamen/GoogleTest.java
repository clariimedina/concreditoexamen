package concreditoExamen;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import concreditoExamenPom.GooglePage;

public class GoogleTest {
	
	private WebDriver driver;
	private String urlGoogle = "https://www.google.com";
	private GooglePage googlePage;
	
	@Before
	public void setup() {
		googlePage = new GooglePage(driver);
		driver = googlePage.chromeWebDriverConnection();
		driver.manage().window().maximize();
		driver.get(urlGoogle);
	}
	
	@Test
	public void test() {
		googlePage.searchSomething();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		assertNotSame(urlGoogle, driver.getCurrentUrl());
	}
	
	@After
	public void tearDown() {
		driver.quit();
	}

}
 