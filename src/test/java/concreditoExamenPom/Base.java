package concreditoExamenPom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Base {
	private WebDriver driver;
	
	public Base(WebDriver driver) {
		this.driver = driver;
	}
	
	public WebDriver chromeWebDriverConnection() {
		System.setProperty("webdriver.chrome.driver", "./src/test/resources/chromedriver/chromedriver" );
		driver = new ChromeDriver();
		
		return driver;
	}
	
	public WebElement findElement(By locator) {
		return driver.findElement(locator);
	}
	
	public void click(By locator) {
		driver.findElement(locator).click();
	}
	
	public void clear(By locator) {
		driver.findElement(locator).clear();
	}
	
	public void submit(By locator) {
		driver.findElement(locator).submit();
	}
	public void type(String text, By locator) {
		driver.findElement(locator).sendKeys(text);
	}


}
