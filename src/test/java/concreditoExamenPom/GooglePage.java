package concreditoExamenPom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class GooglePage extends Base {

	By searchBox = By.name("q");

	public GooglePage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	public void searchSomething() {
		clear(searchBox);
		type("concredito", searchBox);
		submit(searchBox);
	}
	
	

}
